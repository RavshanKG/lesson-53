import React from 'react';

const Add = (props) => {
        return (
          <div className="AddTaskForm">
              <input className="input" onChange={props.changeText} type="text" value={props.text}/>
              <button className="button" onClick={props.addTask}>Add</button>
          </div>
        )
    }

export default Add;