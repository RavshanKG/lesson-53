import React from 'react';

const Task = (props) => {
    return (
        <div>
            <p className="task">{props.text}</p>
            <button onClick={props.deleteTask}>Delete</button>
        </div>
    )
};

export default Task;