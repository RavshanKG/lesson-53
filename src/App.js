import React, { Component } from 'react';
import Add from './Add.js';
import Task from './Task.js';
import './App.css';

class App extends Component {
    state = {
        tasks: [
            {text: 'Have breakfast', id: 1},
            {text: 'Go to market', id: 2},
            {text: 'Come back home', id: 3}
        ],
        inputText: ''
    };

    changeText = (event) => {
        this.setState({inputText:event.target.value});
    };

    addTask = () => {
        const task = {text: this.state.inputText, id: Date.now()};
        const tasks = [...this.state.tasks];
        tasks.push(task);

        this.setState({tasks});
    }

    deleteTask = (id) => {
        const tasks = [...this.state.tasks];
        const index = tasks.findIndex(task => task.id === id);

        tasks.splice(index, 1);

        this.setState({tasks});
    }

  render() {
    return (


      <div className="App">
          <Add changeText={this.changeText} text={this.state.inputText} addTask={this.addTask}/>
          {this.state.tasks.map(task => <Task text={task.text} deleteTask={() => this.deleteTask(task.id)} />)}
      </div>
    );
  }
}

export default App;
